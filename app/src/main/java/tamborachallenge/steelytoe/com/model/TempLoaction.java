package tamborachallenge.steelytoe.com.model;


/**
 * Created by hadi on 03/02/2017.
 */

public class TempLoaction {

    // Labels table name
    public static final String TABLE = "temp_location";

    // Labels Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_lat = "lat";
    public static final String KEY_lng = "lng";
    public static final String KEY_timer = "timer";

    // property help us to keep data
    public int id;
    public String lat;
    public String lng;
    public String timer;

}
