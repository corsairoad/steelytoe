package tamborachallenge.steelytoe.com.util;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;

import tamborachallenge.steelytoe.com.common.Impl.CrudTempLocationImpl;
import tamborachallenge.steelytoe.com.common.Maths;
import tamborachallenge.steelytoe.com.common.Strings;
import tamborachallenge.steelytoe.com.loggers.FileLoggerFactory;
import tamborachallenge.steelytoe.com.model.TempLoaction;

/**
 * Created by fadlymunandar on 6/1/17.
 */

public class LocDao {

    private Context context;
    private CrudTempLocationImpl crudTempLocation;
    private TempLoaction tempLoaction;
    private boolean addNewTrackSegment;
    private String dateTimeString;
    private SimpleDateFormat sdf;

    public LocDao(Context context) {
        this.context = context;
        crudTempLocation = new CrudTempLocationImpl(context);
        tempLoaction = new TempLoaction();
        sdf = new SimpleDateFormat("DD:MM:yy HH:mm:ss");
        dateTimeString = sdf.format(new Date());
    }

    public void insertLoc(Location loc, String timeOfEvent) {
        int _Id=0;

        tempLoaction.lat = String.valueOf(loc.getLatitude());
        tempLoaction.lng = String.valueOf(loc.getLongitude());
        tempLoaction.timer = timeOfEvent;
        tempLoaction.id=_Id;

        if (_Id == 0 ){
            _Id = crudTempLocation.insert(tempLoaction);
        }

    }

    public void insertToGpxFile(Location loc, String timer){
        try {
            // Check Folder ~files
            File myDir = new File(context.getFilesDir().getAbsolutePath());
            if (!myDir.exists()){
                myDir.mkdir();
            }

            //Check File ~log.gpx di folder ~files
            File file = new File(myDir+"/log.gpx");
            if (!file.exists()){
                FileOutputStream fOut = context.openFileOutput("log.gpx", Context.MODE_PRIVATE);
                fOut.write(getBeginningXml(dateTimeString).getBytes());
                fOut.write("<trk>".getBytes());
                fOut.write(getEndXml().getBytes());
                fOut.flush();
                fOut.close();
                addNewTrackSegment = true;

                if(loc!=null){
                    long length = new File(context.getFilesDir().getAbsolutePath() + "/log.gpx").length();

                    int offsetFromEnd = (addNewTrackSegment) ? getEndXml().length() : getEndXmlWithSegment().length();
                    long startPosition = length - offsetFromEnd;
                    String trackPoint = getTrackPointXml(loc, timer);

                    RandomAccessFile raf = new RandomAccessFile(myDir + "/log.gpx", "rw");
                    raf.seek(startPosition);
                    raf.write(trackPoint.getBytes());
                    raf.close();

                    /*Log.d(TAG, "length " + length);
                    Log.d(TAG, "getBeginningXml " + getBeginningXml(dateTimeString).length());
                    Log.d(TAG, "getEndXml().length() " + getEndXml().length());
                    Log.d(TAG, "getEndXmlWithSegment().length() " + getEndXmlWithSegment().length());
                    Log.d(TAG, "offsetFromEnd " + offsetFromEnd);
                    Log.d(TAG, "startPosition " + startPosition);
                    Log.d(TAG, "raf " + raf);*/
                    addNewTrackSegment = false;
                }

            } else {
                long length = new File(context.getFilesDir().getAbsolutePath() + "/log.gpx").length();

                int offsetFromEnd = (addNewTrackSegment) ? getEndXml().length() : getEndXmlWithSegment().length();
                long startPosition = length - offsetFromEnd;
                String trackPoint = getTrackPointXml(loc, timer);

                RandomAccessFile raf = new RandomAccessFile(myDir + "/log.gpx", "rw");
                raf.seek(startPosition);
                raf.write(trackPoint.getBytes());
                raf.close();

                /*Log.d(TAG, "length " + length);
                Log.d(TAG, "getBeginningXml " + getBeginningXml(dateTimeString).length());
                Log.d(TAG, "getEndXml().length() " + getEndXml().length());
                Log.d(TAG, "getEndXmlWithSegment().length() " + getEndXmlWithSegment().length());
                Log.d(TAG, "offsetFromEnd " + offsetFromEnd);
                Log.d(TAG, "startPosition " + startPosition);
                Log.d(TAG, "raf " + raf);*/
                addNewTrackSegment = false;
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //Write Location
    public void writeToFile(Location loc) {
        try {
            FileLoggerFactory.write(context.getApplicationContext(), loc);
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }


    public String getBeginningXml(String dateTimeString){
        StringBuilder initialXml = new StringBuilder();
        initialXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
        initialXml.append("<gpx version=\"1.0\" creator=\"GPSLogger Hadi - http://gpslogger.hadi.com/\" ");
        initialXml.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ");
        initialXml.append("xmlns=\"http://www.topografix.com/GPX/1/0\" ");
        initialXml.append("xsi:schemaLocation=\"http://www.topografix.com/GPX/1/0 ");
        initialXml.append("http://www.topografix.com/GPX/1/0/gpx.xsd\">");
        initialXml.append("<time>").append(dateTimeString).append("</time>");
        return initialXml.toString();
    }

    private String getEndXml(){
        return "</trk></gpx>";
    }

    String getEndXmlWithSegment(){
        return "</trkseg></trk></gpx>";
    }

    String getTrackPointXml(Location loc,String timer) {

        StringBuilder track = new StringBuilder();

        if (addNewTrackSegment) {
            track.append("<trkseg>");
        }

        track.append("<trkpt lat=\"")
                .append(String.valueOf(loc.getLatitude()))
                .append("\" lon=\"")
                .append(String.valueOf(loc.getLongitude()))
                .append("\">");

        if (loc.hasAltitude()) {
            track.append("<ele>").append(String.valueOf(loc.getAltitude())).append("</ele>");
        }

        track.append("<time>").append(timer).append("</time>");

        if (loc.hasBearing()) {
            track.append("<course>").append(String.valueOf(loc.getBearing())).append("</course>");
        }

        if (loc.hasSpeed()) {
            track.append("<speed>").append(String.valueOf(loc.getSpeed())).append("</speed>");
        }

        if (loc.getExtras() != null) {
            String geoidheight = loc.getExtras().getString("GEOIDHEIGHT");

            if (!Strings.isNullOrEmpty(geoidheight)) {
                track.append("<geoidheight>").append(geoidheight).append("</geoidheight>");
            }
        }

        track.append("<src>").append(loc.getProvider()).append("</src>");


        if (loc.getExtras() != null) {

            int sat = Maths.getBundledSatelliteCount(loc);

            if(sat > 0){
                track.append("<sat>").append(String.valueOf(sat)).append("</sat>");
            }


            String hdop = loc.getExtras().getString("HDOP");
            String pdop = loc.getExtras().getString("PDOP");
            String vdop = loc.getExtras().getString("VDOP");
            String ageofdgpsdata = loc.getExtras().getString("AGEOFDGPSDATA");
            String dgpsid = loc.getExtras().getString("DGPSID");

            if (!Strings.isNullOrEmpty(hdop)) {
                track.append("<hdop>").append(hdop).append("</hdop>");
            }

            if (!Strings.isNullOrEmpty(vdop)) {
                track.append("<vdop>").append(vdop).append("</vdop>");
            }

            if (!Strings.isNullOrEmpty(pdop)) {
                track.append("<pdop>").append(pdop).append("</pdop>");
            }

            if (!Strings.isNullOrEmpty(ageofdgpsdata)) {
                track.append("<ageofdgpsdata>").append(ageofdgpsdata).append("</ageofdgpsdata>");
            }

            if (!Strings.isNullOrEmpty(dgpsid)) {
                track.append("<dgpsid>").append(dgpsid).append("</dgpsid>");
            }
        }



        track.append("</trkpt>\n");

        track.append("</trkseg></trk></gpx>");

        return track.toString();
    }

}
