package tamborachallenge.steelytoe.com.common.events;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.format.DateFormat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationServices;

import java.text.SimpleDateFormat;
import java.util.Date;

import tamborachallenge.steelytoe.com.util.LocDao;

/**
 * Created by fadlymunandar on 6/1/17.
 */

public class ActRecogIntentService extends IntentService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = ActRecogIntentService.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private LocDao locDao;
    private DetectedActivity detectedActivity;

    public ActRecogIntentService() {
        super(TAG);
        Log.d(TAG, "Activity Recognition Api started");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locDao = new LocDao(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            Log.d(TAG, "Activity recognition result detected");
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            detectedActivity = result.getMostProbableActivity();

            int probability = detectedActivity.getConfidence();
            int actType = detectedActivity.getType();
            String actTypeString = toTypeString(actType);

            Log.d(TAG, "CONFIDENCE: " + probability);
            Log.d(TAG, "ACT TYPE int: " + actType);
            Log.d(TAG, "ACT TYPE String: " + actTypeString);

            if (isSatisfied(actType, probability)){
                mGoogleApiClient.connect();
            }


        } else {
            Log.d(TAG, "Activity recognition has no result");
        }
    }

    private String toTypeString(int typeInt) {
        switch (typeInt) {
            case DetectedActivity.IN_VEHICLE:
                return "IN_VEHICLE";
            case DetectedActivity.ON_BICYCLE:
                return "ON_BICYCLE";
            case DetectedActivity.RUNNING:
                return "RUNNING";
            case DetectedActivity.ON_FOOT:
                return "ON_FOOT";
            case DetectedActivity.STILL:
                return "STILL";
            case DetectedActivity.WALKING:
                return "WALKING";
            case DetectedActivity.UNKNOWN:
                return "UNKNOWN";
            case DetectedActivity.TILTING:
                return "TILTING";
            default:
                return "EMPTY";

        }
    }

    private boolean isSatisfied(int actype, int confidence){
        boolean isActype = true;

        switch (actype){
            case DetectedActivity.STILL:
                isActype = false;
                break;
            case DetectedActivity.UNKNOWN:
                isActype = false;
                break;
            default:
                break;
        }

        return isActype && confidence > 10;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String timeFormat = sdf.format(date);

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        Log.d(TAG,"Lat: " + location.getLatitude());
        Log.d(TAG,"Lon: " + location.getLongitude());

        locDao.insertLoc(location,timeFormat);
        locDao.insertToGpxFile(location, timeFormat);
        locDao.writeToFile(location);


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
